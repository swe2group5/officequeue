const dao = require('../dao');

test('List of request types is non-empty', async () => {
    const services = await dao.queueList();
    expect(services.length).toBeGreaterThan(0);
});

test('List of counters is non-empty', async () => {
    const counters = await dao.counterList();
    expect(counters.length).toBeGreaterThan(0);
});

test('New ticket is valid', async () => {
    const queues = await dao.queueList();
    const services = queues[0].req_type;

    const ticket = await dao.insertTicket({req_type: services});
    expect(ticket.req_type).toBe(services);
    expect(ticket.waiting_time).toBeGreaterThanOrEqual(0);
    await dao.reset();
});

test('New ticket is added to the correct queue', async () => {
    const services = await dao.queueList();
    const reqType = services[0].req_type;

    const queuesBefore = await dao.queueCount();
    let countBefore = 0;
    if(queuesBefore){
        for(const queue of queuesBefore){
            if(queue.req_type === reqType)
                countBefore = queue.n;
        }
    }
    
    const ticket = await dao.insertTicket({req_type: reqType});

    const queuesAfter = await dao.queueCount();
    let countAfter = 0;
    for(const queue of queuesAfter){
        if(queue.req_type === reqType)
            countAfter = queue.n;
    }
    expect(countAfter).toBe(countBefore+1);
    await dao.reset();
});

test('Next ticket is selected from the longest queue', async () => {
    await dao.reset();
    const services = await dao.queueList();
    const reqType1 = services[0].req_type;
    const reqType2 = services[1].req_type;
    const counters = await dao.counterList();

    const ticket1 = await dao.insertTicket({req_type: reqType1});
    const ticket2 = await dao.insertTicket({req_type: reqType2});
    const ticket3 = await dao.insertTicket({req_type: reqType2});

    //counter[0] serves both type1 and type2
    const nextTicket = await dao.nextTicket({counter_id: counters[0].counter_id});
    expect(nextTicket.req_type).toBe(reqType2);
    await dao.reset();
});

test('Reset removes all tickets from the queues and resets display ticket id', async () => {
    const services = await dao.queueList();
    const reqType = services[0].req_type;

    const ticket = await dao.insertTicket({req_type: reqType});
    await dao.reset();
    const queuesAfter = await dao.queueCount();
    expect(queuesAfter.length).toBe(0);

    const ticketAfter = await dao.insertTicket({req_type: reqType});
    expect(ticketAfter.display_id).toBe(1);
    await dao.reset();
});