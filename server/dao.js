'use strict';

const sqlite=require('sqlite3').verbose();
//const bcrypt=require('bcrypt');

//apro la connessione col db
const db=new sqlite.Database('./office_queue',(err)=>{
    if(err){
         console.error(err.message);
         throw err;
    }
 });

exports.insertTicket= async function(params){
    const timestamp=new Date().getTime();  
    let query_info=await getQueueInfo(params.req_type);
    if(!query_info){
        query_info={n:0,tr:0};
    }
    //console.log(JSON.stringify(query_info));
    let baseid = await getBaseId();
    let CounterSum=await getCounterSum(params.req_type);
    //console.log(JSON.stringify(CounterSum));
    let req_name=await getReqName(params.req_type);
    //console.log(req_name.name);
    const waiting_time=query_info.tr*(query_info.n/CounterSum+0.5); 
    //console.log(waiting_time);
    return new Promise( (resolve,reject)=>{
    const sql="INSERT INTO queue_log (timestamp,timestamp_served,req_type,served,counter,waiting_time)  VALUES(?,'NULL',?,0,0,?)";   
    db.run(sql,[timestamp,params.req_type,waiting_time],
        function(err){
            if(err){
                reject(err);
            }
            else
                resolve({id:this.lastID, display_id:this.lastID-baseid, timestamp:timestamp,req_type:params.req_type,req_name:req_name.name,waiting_time:waiting_time});
        });
    });
}

async function getQueueInfo(req_type){
    return new Promise((resolve,reject)=>{
    const query="SELECT service.id as req_type,tr,count (*) as n from service,queue_log where service.id=queue_log.req_type and served=0 and req_type=? group by service.id,tr";
    db.get(query,[req_type],(err,row)=>{
        
            if(err)
                reject(err);
            else{
                resolve(row);
            }
        });
    });
}

async function getReqName(req_type){
    return new Promise((resolve,reject)=>{
    const query="select name from service where id=?";
    db.get(query,[req_type],(err,row)=>{
        
            if(err)
                reject(err);
            else{
                resolve(row);
            }
        });
    });
}

async function getBaseId(){
    return new Promise((resolve,reject)=>{
    const query="SELECT baseid FROM ticket_base";
    db.get(query, (err,row)=>{
            if(err){
                reject(err);
            }else{
                resolve(row.baseid);
            }
        });
    });
}

async function getCounterSum(req_type){
    return new Promise((resolve,reject)=>{
    const query="SELECT counter_id, COUNT(*) as k from counter where counter_id in (select counter_id from counter where service_id=?) group by counter_id ";
    db.all(query,[req_type],(err,rows)=>{       
                if(err)
                    reject(err);
                else{
                    let sum=0;
                    for(let row of rows){
                        sum+=row.k;
                    }
                    resolve(sum);
                }
            });          
     });
}

exports.queueList=function(){
    return new Promise((resolve,reject)=>{
        const query="SELECT service.id as req_type,name,color from service";
        db.all(query,(err,rows)=>{
            if(err)
                reject(err);
            else {
                const queue_array=[];
                for(let row of rows){
                    queue_array.push({
                        req_type:row.req_type,
                        name:row.name,
                        color:row.color
                    });
                }
                resolve(queue_array);
            }
        });
    });
}

exports.queueCount=function(){
    return new Promise((resolve,reject)=>{
        const query="SELECT service.id as req_type,name,tr,count (*) as n,color from service,queue_log where service.id=queue_log.req_type and served=0 group by service.id,name,tr";
        db.all(query,(err,rows)=>{
            if(err)
                reject(err);
            else {
                const queue_array=[];
                for(let row of rows){
                    queue_array.push({
                        req_type:row.req_type,
                        name:row.name,
                        n:row.n,
                        color:row.color
                    });
                }
                resolve(queue_array);
            }
        });
    });
}

exports.counterList=function(){
    return new Promise((resolve,reject)=>{
        const query="SELECT counter_id, service_id,name,color from counter,service where counter.service_id=service.id ORDER BY counter_id";
        db.all(query,(err,rows)=>{
            if(err)
                reject(err);
            else {
                const counter_array=[];
                for(let row of rows){
                    if(counter_array[row.counter_id-1]){
                        counter_array[row.counter_id-1].services.push({service_id:row.service_id,name:row.name, color:row.color});
                    }else{
                        counter_array.push({
                            counter_id:row.counter_id,
                            services:[{service_id:row.service_id,name:row.name,color:row.color}]
                        });
                    }
                }
                resolve(counter_array);
            }
        });
    });
}


function serveTicket(counter_id){
    return new Promise( (resolve,reject)=>{
        const sql="UPDATE queue_log SET served=2, timestamp_served=? WHERE counter=? and served=1";
        
        db.run(sql,[new Date().getTime(),counter_id],
            function(err){
                if(err){
                    reject(err);
                }
                else{
                    resolve("OK");
                }
            });
    });
}

function startServeTicket(counter_id,id){
    return new Promise( (resolve,reject)=>{
        const sql="UPDATE queue_log SET served=1, counter=? WHERE id=?";
        db.run(sql,[counter_id,id],
            function(err){
                if(err){
                    reject(err);
                }
                else{
                    resolve("OK");
                }
            });
    });
}

exports.nextTicket=function(params){
    
    return new Promise((resolve,reject)=>{
        serveTicket(params.counter_id).then((o)=>{
            getBaseId().then((baseid) => {
                const query="SELECT min(queue_log.id)as ticket_id,counter.counter_id as counter_id,"+
                "service.id as req_type,name,tr,count (*) as n from service,queue_log,counter "+
                "where service.id=queue_log.req_type and service.id=counter.service_id "+
                "and served=0 and counter.counter_id=? "+ 
                "group by counter.counter_id,service.id,name";

                db.all(query,[params.counter_id],(err,rows)=>{
                    if(err)
                        reject(err);
                    else {
                        let cur_row=undefined;
                        for(let row of rows){
                            if(!cur_row)
                                cur_row=row;
                            else if(row.n>cur_row.n){
                                cur_row=row;
                            }else if(row.n==cur_row.n && row.tr<cur_row.tr)
                                cur_row=row;
                        }

                        if(cur_row){
                            startServeTicket(cur_row.counter_id,cur_row.ticket_id).then( ()=>{  
                                cur_row.display_id = cur_row.ticket_id - baseid;
                                resolve(cur_row);
                            }).catch((err)=>{
                                reject(err);
                            });
                        }else
                            resolve({});
                    }
                });
            })
        }).catch((err)=>{
        reject(err);
        });
    });
}

exports.reset = () => {
    return new Promise((resolve,reject)=>{
        removePendingTickets()
        .then(() => updateBaseTicketID())
        .then(() => resolve())
        .catch(err => reject(err));
    });
}

function removePendingTickets(){
    return new Promise((resolve,reject)=>{
        const query = "UPDATE queue_log SET served=3 WHERE served<>2";
        db.run(query, (err) => {
            if(err)
                reject(err);
            else{
                resolve();
            }
        });
    });
}

function updateBaseTicketID(){
    return new Promise((resolve,reject)=>{
        const query = "UPDATE ticket_base SET baseid=(SELECT MAX(id) FROM queue_log)";
        db.run(query, (err) => {
            if(err)
                reject(err);
            else{
                resolve();
            }
        });
    });
}


