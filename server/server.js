const express = require('express');
const dao = require('./dao');
const morgan = require('morgan');
const jwt=require('express-jwt');
const moment=require('moment');


const PORT = 3001;

const app = express();

app.use(morgan('tiny'));
app.use(express.json());



app.get('/api/queue_list',(req,res)=>{
    dao.queueList()
        .then((queue_list)=>{
            res.json(queue_list);
        })
        .catch((err)=>{
            res.status(400).json({
                errors:[{'msg':err}]
            });
        });
});

app.get('/api/queue_count',(req,res)=>{
    dao.queueCount()
        .then((queue_list)=>{
            res.json(queue_list);
        })
        .catch((err)=>{
            res.status(400).json({
                errors:[{'msg':err}]
            });
        });
});

app.get('/api/counter_list',(req,res)=>{
    dao.counterList()
        .then((queue_list)=>{
            res.json(queue_list);
        })
        .catch((err)=>{
            res.status(400).json({
                errors:[{'msg':err}]
            });
        });
});

app.post('/api/ticket/:req_type',(req,res)=>{
    let params={
        req_type:req.params.req_type
    }
    dao.insertTicket(params).then((obj)=>{
        res.json(obj);
    }).catch((e)=>{
        res.status(400).json({errors:[{'param':'Server','msg':e}]});
    });
});

app.post('/api/reset',(req,res)=>{
    dao.reset().then(()=>{
        res.status(200).json("OK");
    }).catch((e)=>{
        console.log(e);
        res.status(400).json({errors:[{'param':'Server','msg':e}]});
    });
});

app.put('/api/serviceRequest/:counter_id',(req,res)=>{
   
    let params={
        counter_id:req.params.counter_id
    }
    dao.nextTicket(params).then((obj)=>{
        res.json(obj);
    }).catch((e)=>{
        res.status(400).json({errors:[{'param':'Server','msg':e}]});
    });
    
});

app.listen(PORT, ()=>console.log(`Server running on http://localhost:${PORT}/`));