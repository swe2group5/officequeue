import React from 'react';
import moment from 'moment';
import API from './API';

class Tickets extends React.Component{
    constructor(props){
        super(props);
        this.state={queue_list:[],ticket_list:[]};
    }

    componentDidMount(){
        API.getQueueList().then(queues => {
            this.setState({queue_list: queues});
        }).catch(err => console.log(err));
    }

    getTicket=(req_type)=>{
        API.addTicket(req_type).then((ticket)=>{
            let prevlist=[...this.state.ticket_list];
            this.setState({ticket_list:[ticket,...prevlist]});
        }).catch(err => console.log(err));
    }

    render(){
        return (
            <div className='container'>
                <h1>Tickets</h1>
                <i>Select a service</i>
                <div className="row">
                    {
                        this.state.queue_list.map((e)=>{
                        return( 
                            <div className="col-2" key={e.req_type}>
                                <button type='button' className="btn-lg btn-block" style={{backgroundColor:e.color}} onClick={()=>{this.getTicket(e.req_type)}}>{e.name}</button>
                            </div>)
                        })
                    }
                </div>
                <div className="row">
                    <table className='table'  style={{"width":"900px"}}>
                        <tbody>
                            {
                                this.state.ticket_list.map((e)=>{
                                    return( 
                                        <tr key={e.id}>
                                            <td>
                                                Ticket Number: {e.display_id}
                                            </td>
                                            <td>
                                                Timestamp: {moment(e.timestamp).format('DD/MM/YYYY hh:mm:ss')}
                                            </td>
                                            <td>
                                                Request Type: {e.req_name}
                                            </td>
                                            <td>
                                                Waiting time: {Math.floor(e.waiting_time/60)}m {Math.floor(e.waiting_time)%60}s
                                            </td>
                                           
                                        </tr>);}) 
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }

}

export default Tickets;