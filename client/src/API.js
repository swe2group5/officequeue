
async function addTicket(req_type) {
    const response = await fetch("/api/ticket/"+req_type,{
        method:'POST',
        headers:{
            'Content-Type':'application/json'
        }
    });
    const ticket = await response.json();

    if(response.ok){
        return ticket;
    } else{
        let err = {status: response.status, errObj:ticket};
        throw err;
    }
}

async function getQueueList() {
    const response = await fetch("/api/queue_list");
    const queues = await response.json();
    if(response.ok){
        return queues;
    } else {
        let err = {status: response.status, errObj:queues};
        throw err;
    }
}

async function getCounterList() {
    const response = await fetch("/api/counter_list");
    const counters = await response.json();
    if(response.ok){
        return counters;
    } else {
        let err = {status: response.status, errObj:counters};
        throw err;
    }
}

async function getQueueCount() {
    const response = await fetch("/api/queue_count");
    const queues = await response.json();
    if(response.ok){
        return queues; 
    } else {
        let err = {status: response.status, errObj:queues};
        throw err;
    }
}

async function serviceRequest(counter_id) {
    const response = await fetch("/api/serviceRequest/"+counter_id,{
        method:'PUT',
        headers:{
            'Content-Type':'application/json'
        }
    });
    const ticket = await response.json();

    if(response.ok)
        return ticket
    else{
        let err = {status: response.status, errObj:ticket};
        throw err;
    }
}

async function reset() { 
    const response = await fetch("/api/reset",{
        method:'POST',
        headers:{
            'Content-Type':'application/json'
        }
    });

    if(!response.ok) {
        let err = {status: response.status, errObj:{}};
        throw err;
    }
}

const API = { addTicket, getQueueList, getCounterList, getQueueCount, serviceRequest, reset };
export default API;