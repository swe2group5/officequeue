import React from 'react';
import API from './API';

class Display extends React.Component{
    constructor(props){
        super(props);
        this.state={queue_list:[],counter_list:[], counter_checks:[]};
    }

    async componentDidMount(){
        let sup = await API.getQueueList();
        sup.sort((e1,e2)=>{
            return e1.req_type-e2.req_type;
        });
        this.setState({queue_list:sup.map((e)=>{return {...e,n:0}})});
        
        const counters = await API.getCounterList();
        const counter_checks = counters.map((counter) => false);
        this.setState({counter_list:counters, counter_checks:counter_checks});
        
        let self=this;
        setInterval(async function(){
            let new_q = await API.getQueueCount();

            let flag=0;
            for(let i=0; i< self.state.queue_list.length; i++){
                for(let j=0;j< new_q.length;j++){
                    if(self.state.queue_list[i].req_type===new_q[j].req_type){
                        flag=1;
                        break;
                    }
                }
                
                if(flag==0){
                    new_q.push({
                        req_type:self.state.queue_list[i].req_type,
                        name:self.state.queue_list[i].name,
                        color:self.state.queue_list[i].color,
                        n:0
                    });
                }
                flag=0;
            }
            new_q.sort((e1,e2)=>{
                return e1.req_type-e2.req_type;
            });
            self.setState({queue_list:new_q});
        }
        ,1000);
    }

    callNext=(counter_id)=>{
        API.serviceRequest(counter_id)
        .then((obj)=>{
            let new_c=[...this.state.counter_list];
            new_c[counter_id-1].ticket_id=obj.display_id;
            this.setState({counter_list:new_c});
        });
    }

    reset = () => {
        API.reset().then(() => {
            const queues = this.state.queue_list;
            queues.forEach(q => q.n = 0);
            this.setState({queue_list: queues});
        })
        .catch(err => console.log(err));
    }

    check = (event, index) => {
        const checks = this.state.counter_checks;
        const new_checks = checks.map(check => false);
        new_checks[index] = event.target.checked;
        this.setState({counter_checks: new_checks});
    }

    render(){
        return (
            <>
            <div className='container' >
                <h1>Queues</h1>
                <div className="row">
                    {
                        this.state.queue_list.map((e)=>{
                            return( 
                                <div className="col-3" style={{backgroundColor:e.color}}>
                                    <h1>{e.name}</h1>
                                    <h2>{e.n}</h2>
                                    </div>
                                );}) 
                    }
                </div>
                <hr/>
                <h1>Counters</h1>
                <div className="row">
                {
                    this.state.counter_list.map((e, i)=>{
                        return( 
                            <div className="col-3 text-center">
                                <input type="checkbox" checked={this.state.counter_checks[i]} onChange={(event) => this.check(event, i)}/>
                                <div>
                                    <h4>{"Counter " + e.counter_id}</h4>
                                    <p>
                                        now serving: #{e.ticket_id?e.ticket_id:"none"}
                                    </p>
                                </div>
                                <div>
                                    {
                                        e.services.map((ein)=>{return(
                                            <p style={{color:ein.color}}>
                                                {ein.name}
                                            </p>
                                        )})
                                    }
                                </div>
                                {this.state.counter_checks[i] && 
                                <div>
                                    <button type='button' className='btn btn-primary btn-lg' onClick={()=>{this.callNext(e.counter_id)}} >Call</button><br/>
                                    <button type='button' className='btn btn-warning btn-lg' onClick={()=>{this.reset()}} >Reset</button>
                                </div>}
                            </div>);}) 
                } 
                </div>
            </div>
            </>
        );
    }

}

export default Display;